import ConfigParser

class ScriptConfig(dict):
    "Read the configuration file"
    def __init__(self, filename=None):
        dict.__init__(self)

        self._error = False
        self._errorStr = ""
        self._mailServer = 'MailServer'
        self._mailServerPort = "ServerPort"
        self._mailUseTLS = 'UseTLS'
        self._sshUser = 'SshUser'
        self._remoteDirectory = 'RemoteDirectory'
        self._localDirectory = 'LocalDirectory'
        self._mailRecipients = 'MailRecipients'
        self._mailLoginUser = 'MailLoginUser'
        self._mailLoginPassword = 'MailLoginPassword'
        self._mailSenderName = 'MailSenderName'
        self._mailSenderAddress = 'MailSenderAddress'

        config = ConfigParser.ConfigParser()
        # Try to open the configuration file
        try:
            fp = open(filename)
            config.readfp(fp)
        except IOError:
            self._error = True
            self._errorStr = "Could not open configuration file: " + filename
            return
        # Try to retrieve the properties that we expect
        try:
            settingsSection = 'Settings'
            self[self._mailServer] = config.get(settingsSection, self._mailServer)
            self[self._mailServerPort] = config.get(settingsSection, self._mailServerPort)
            self[self._mailUseTLS] = config.get(settingsSection, self._mailUseTLS)
            self[self._sshUser] = config.get(settingsSection, self._sshUser)
            self[self._remoteDirectory] = config.get(settingsSection, self._remoteDirectory)
            self[self._localDirectory] = config.get(settingsSection, self._localDirectory)
            self[self._mailRecipients] = config.get(settingsSection, self._mailRecipients)
            self[self._mailLoginUser] = config.get(settingsSection, self._mailLoginUser)
            self[self._mailLoginPassword] = config.get(settingsSection, self._mailLoginPassword)
            self[self._mailSenderName] = config.get(settingsSection, self._mailSenderName)
            self[self._mailSenderAddress] = config.get(settingsSection, self._mailSenderAddress)
        except ConfigParser.NoSectionError as e:
            self._error = True
            self._errorStr = e.message + " was found in " + filename
            return
        except ConfigParser.NoOptionError as e:
            self._error = True
            self._errorStr = e.message + " in configuration file '" + filename + "'"
            return
        
    def Error(self):
        "Return True if an error occurred, otherwise False"
        return self._error
    def ErrorStr(self):
        "Return the error that was last encountered, otherwise an empty string"
        return self._errorStr
    def MailServer(self):
        "Get the server name"
        return self[self._mailServer]
    def MailServerPort(self):
        "Get the port to use for the mail server"
        return self[self._mailServerPort]
    def MailUseTLS(self):
        "Use TLS?"
        return self[self._mailUseTLS]
    def SshUser(self):
        "Get the SSH User"
        return self[self._sshUser]
    def RemoteDirectory(self):
        "Get the remote directory"
        return self[self._remoteDirectory]
    def LocalDirectory(self):
        "Get the local directory"
        return self[self._localDirectory]
    def MailRecipients(self):
        "Get the mail recipients"
        s = self[self._mailRecipients].split()
        return s
    def MailLoginUser(self):
        "Get the user name to login with to the mail server"
        return self[self._mailLoginUser]
    def MailLoginPassword(self):
        "Get the password to login with to the mail server"
        return self[self._mailLoginPassword]
    def MailSenderName(self):
        "Return the name of the sender of the email"
        return self[self._mailSenderName]
    def MailSenderAddress(self):
        "Return the address of the sender of the email"
        return self[self._mailSenderAddress]
