class StringOut(object):
    """
    Utility class to write a string both to the console as to a string that is
    maintained by this class. At all points the collected string can be
    accessed with a call to Message()
    """
    def __init__(self):
        "Initialise: create an empty log string"
        object.__init__(self)
        self._logList = []
    def out(self, message):
        "print the incoming message and add it to the log string at the same time"
        print message
        self._logList.append(str(message))
    def Message(self):
        "return the log string"
        logStr = '\n'.join(self._logList)
        return logStr
