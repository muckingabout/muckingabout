'''
Created on 10 Oct 2012

@author: nly99059
'''
import smtplib
import email.utils
from email.mime.text import MIMEText

class SendMail(object):
    "Send email"
    def __init__(self, mailServer, useTls, port, mailServerLogin=None, mailServerPassword=None):
        object.__init__(self)
        self._mailServer = mailServer
        self._useTls = useTls
        self._port = port
        self._mailServerLogin = mailServerLogin
        self._mailServerPassword = mailServerPassword
        self._senderName = ''
        self._senderAddress = ''
        self._recipients = []
    def SetSender(self, name, address):
        self._senderName = name
        self._senderAddress = address
    def SetRecipients(self, recipients):
        self._recipients = recipients
    def Send(self, message, subject):
        msg = MIMEText(message, 'plain', 'utf-8')
        msg['Subject'] = subject
        msg['From'] = email.utils.formataddr((self._senderName, self._senderAddress))
        msg['Date'] = email.utils.formatdate()
        for x in self._recipients:
            msg.add_header('To', x)
        
        server = smtplib.SMTP(self._mailServer, self._port, timeout=30)
        server.set_debuglevel(True)
        try:
            if self._useTls:
                server.starttls()
                server.login(self._mailServerLogin, self._mailServerPassword)
            to_addrs = msg.get_all('To')
            server.sendmail(self._senderAddress, to_addrs, msg.as_string())
        finally:
            server.quit() 
