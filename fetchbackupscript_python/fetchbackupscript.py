#! /usr/bin/env python
# coding: utf-8

import sys
import traceback
import socket
from ScriptConfig import ScriptConfig
from StringOut import StringOut
from SendMail import SendMail
from time import strftime, localtime
import os.path
import subprocess
import argparse

emailSender = 'DiskStation'
so = StringOut()


def abort(message):
    """
    Try to send an e-mail without the settings from the configuration file. We
    have to try this since there was an error reading the configuration file.
    """
    try:
        mail = SendMail("mbvelden.nl", False, 25)
        mail.SetSender(emailSender, emailSender)
        mail.SetRecipients(['admin@mbvelden.nl'])
        mail.Send(message, 'Fetch backup failed')
    except Exception as e:
        so.out("SendMail in abort failed: " + str(e))
        so.out("\n")
        so.out(traceback.format_exc())
    finally:
        sys.exit()

def mail(config, subject, message):
    mail = SendMail(config.MailServer(), config.MailUseTLS(), config.MailServerPort(),
                    config.MailLoginUser(), config.MailLoginPassword())
    mail.SetSender(config.MailSenderName(), config.MailSenderAddress())
    mail.SetRecipients(config.MailRecipients())
    mail.Send(message, subject)
        
def day_of_week_as_text(date):
    return strftime("%A", date)

now = localtime()
dayofweek = now.tm_wday + 1

try:
    parser = argparse.ArgumentParser(description='Fetch backups on the mbvelden.nl VPS')
    parser.add_argument('configFile', metavar='cfg', type=str,
                       help='the configuration file for the backup fetch script')
    args = parser.parse_args()

    so.out("#####################################################################")
    so.out("Fetching backup for " + day_of_week_as_text(now) + " on: " + socket.getfqdn())
    so.out("Backup configuration file: " + args.configFile)
    so.out("Started at: " + strftime("%Y-%m-%d %H:%M", now))
    so.out("")
    
    config = ScriptConfig(args.configFile)
    if config.Error():
        so.out("Fatal error: " + config.ErrorStr())
        abort(so.Message())
    #    sys.exit()
    so.out("Settings")
    so.out("\tServer: " + config.MailServer())
    so.out("\tPort: " + config.MailServerPort())
    so.out("\tUse TLS: " + config.MailUseTLS())
           
    so.out("\tSSH User: " + config.SshUser())
    so.out("\tRemote directory: " + config.RemoteDirectory())
    so.out("\tLocal directory: " + config.LocalDirectory())
    so.out("\tMail recipients: " + ','.join(config.MailRecipients()))
    
    """
    Setup local directory to receive the backup. The local directory is a
    concatenation of the base local directory with the number of the day of the
    week. This means that a week of backups is kept before the first one is
    overwritten.
    """
    localpath = os.path.join(config.LocalDirectory(), str(dayofweek))
    so.out("Fetch backup to: " + localpath)
    if os.path.exists(localpath) == False:
        os.makedirs(localpath)
    
    rsync_options = '-avzh -e ssh'
    rsync_source = config.SshUser() + ':' + config.RemoteDirectory()
    rsync_command = ['rsync'] + rsync_options.split() + [rsync_source, localpath]
    so.out("rsync_command: " + str(rsync_command))
    
    so.out(subprocess.check_output(rsync_command))
    
    mail(config, 'Fetch backup successful', so.Message())
except Exception as e:
    so.out("\nCaught an unhandled exception: " + str(e))
    so.out("\n")
    so.out(traceback.format_exc())

    mail(config, 'Fetch backup failed', so.Message())    
    #so.out("Trace back: " )
finally:
    outFile = open('log.txt', 'w')
    outFile.write(so.Message())
    outFile.close()
    
