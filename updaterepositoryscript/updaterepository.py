#! /usr/bin/env python
# coding: utf-8

from __future__ import print_function  # Use the modern print variant

import sys
import traceback
import ConfigParser
import argparse
import subprocess
import time
from time import strftime, localtime

class StringOut(object):
    """
    Utility class to write a string both to the console as to a string that is
    maintained by this class. At all points the collected string can be
    accessed with a call to Message()
    """
    def __init__(self):
        "Initialise: create an empty log string"
        object.__init__(self)
        self._logList = []
    def out(self, message):
        "print the incoming message and add it to the log string at the same time"
        print(message)
        self._logList.append(str(message))
    def Message(self):
        "return the log string"
        logStr = '\n'.join(self._logList)
        return logStr

class ScriptConfig(dict):
    """"
    ScriptConfig reads all the configuration parameters from a given configuration
    file
    """ 
    def __init__(self, filename=None):
        "Read the configuration file"
        dict.__init__(self)

        self._error = False
        self._errorStr = ""
        self._repository = 'Repository'
        
        self._config = ConfigParser.ConfigParser()
        # Try to open the configuration file
        try:
            fp = open(filename)
            self._config.readfp(fp)
        except IOError:
            self._error = True
            self._errorStr = "Could not open configuration file: " + filename
            return
        # Try to retrieve the properties that we expect
        try:
            self._settingsSection = 'Settings'
            self._mailSettingsSection = 'MailSettings'
            self[self._repository] = self._config.get(self._settingsSection, self._repository)
        except ConfigParser.NoSectionError as e:
            self._error = True
            self._errorStr = e.message + " was found in " + filename
            return
        except ConfigParser.NoOptionError as e:
            self._error = True
            self._errorStr = e.message + " in configuration file '" + filename + "'"
            return
        
    def Error(self):
        "Return True if an error occurred, otherwise False"
        return self._error
    def ErrorStr(self):
        "Return the error that was last encountered, otherwise an empty string"
        return self._errorStr
    def RedminePath(self):
        "The path to the Redmine installation, i.e. /usr/share/redmine"
        return self._config.get(self._settingsSection, 'RedminePath')
    def Repository(self):
        "Get the repository path"
        return self._config.get(self._settingsSection, 'Repository')
    def RedmineUpdateUrl(self):
        "Get the Redmine update URL"
        return self._config.get(self._settingsSection, 'RedmineUpdateUrl')

def print_popen(proc, commandStr):
    so.out(commandStr)
    res = proc.stdout.read()
    if len(res) == 0:
        res = 'None'
    so.out("stdout: " + str(res))
    res = proc.stderr.read()
    if len(res) == 0:
        res = 'None'
    so.out("stderr: " + str(res))

so = StringOut()
error = False
updated = False
startTime = None

try:
    startTime = time.time()
    so.out(strftime("%Y-%m-%d %H:%M", localtime()) + " - Starting updating the bare git repository")

    # The config file should be passed as argument
    parser = argparse.ArgumentParser(description='Update a bare git repository')
    parser.add_argument('configFile', metavar='config', type=str,
                        help='the configuration file for the update repository script')
    args = parser.parse_args()
    
    config = ScriptConfig(args.configFile)
    if config.Error():
        so.out("Fatal error: " + config.ErrorStr())
        error = True

    if not error:
        so.out("Using repository: " +  config.Repository())
        so.out("Redmine path: " + str(config.RedminePath()))
        # git fetch --all && git reset --soft HEAD
        proc = subprocess.Popen(['git', 'fetch', '--all'], cwd=config.Repository(), shell=False,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print_popen(proc, 'git fetch --all')

        proc = subprocess.Popen(['git', 'reset', '--soft', 'HEAD'], cwd=config.Repository(), shell=False,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print_popen(proc, 'git reset --soft HEAD')

        """ Have Redmine fetch all the git changesets so that all issues are updated with the
            correct git commits
        """ 
        proc = subprocess.Popen(['curl', config.RedmineUpdateUrl()], shell=False)

except Exception as e:
    so.out("\nCaught an unhandled exception: " + str(e))
    so.out(traceback.format_exc())
    error = True

finally:
    elapsedTime = time.time() - startTime;
    so.out("Script took %.3f seconds to run" % elapsedTime)

    outFile = open('updatescript.log', 'w')
    outFile.write(so.Message())
    outFile.close()

    if error:
        sys.exit(1)
