#!/usr/bin/perl
use warnings;
use strict;

use Config::Simple;
use POSIX 'strftime';
use File::Path;

# The message to mail
my $mailMessage;

# Record start time
my $startTime = time();

# Read config file
my $configFile = "config";
my $hostname = `hostname -A`;
print "##########################################################\n";
print "Backing up ", $hostname, "\n";
print "Backup configuration file: ", $configFile, "\n";
my $cfg = new Config::Simple();
$cfg->read($configFile) or die $cfg->error();
my $destination = $cfg->param('destination') or die $cfg->error("destination not set");
my $mySqlUser = $cfg->param('MySQLUser') or die $cfg->error("MySQLUser not set");
my $mySqlPassword = $cfg->param('MySQLPassword') or die $cfg->error("MySQLPassword not set");
my $mySqlDatabases = $cfg->param('MySQLDatabases') or die $cfg->error("MySQLDatabases not set");
my $directories = $cfg->param('directories') or die $cfg->error("directories not set");
my $owner = $cfg->param('owner') or die $cfg->error("owner not set");
# Permission is specified as octal in the config file: convert to decimal
my $permission = oct($cfg->param('permission')) or die $cfg->error("permission not set");
my $emailRecipients = $cfg->param('emailRecipients') or die $cfg->error("emailRecipients not set");

if (not -d $destination) {
    mkpath($destination) or die print "make_path failed";
}

# Prepare the uid and the gid of $owner for later use
(my $name, my $pass, my $uid, my $gid, my $quota, my $comment,
  my $gcos, my $dir, my $shell, my $expire) = getpwnam($owner);

# Get the local date/time
my $dtStr = strftime '%Y-%m-%d %H:%M', localtime;
my $message = "Backup script started at $dtStr\n";
print $message;
$mailMessage .= $message;

# Backup all selected databases
# mysqldump -u root -ppassword --databases mailserver wordpress > mysql_strat.sql
my $mySqlDumpFile = $destination . "/mysql_strat.sql";

$message = "Dumping databases: $mySqlDatabases\n";
print $message;
$mailMessage .= $message;
system("mysqldump -u $mySqlUser -p$mySqlPassword --databases $mySqlDatabases > $mySqlDumpFile");
chown $uid, $gid, $mySqlDumpFile;
chmod $permission, $mySqlDumpFile;

# Set the owner and the permmission of the file


# Backup all the directories (one by one) that are specified
# tar --create --verbose --bzip2 --file=$file.tar.bz2 dirname/
# i.e. tar --create --bzip2 --file var.www.tar.bz2 /var/www/
my @dirs = split(/ /, $directories);
my $element;
my $backupFile;
for $element (@dirs) {
    # Construct destination file name
    $backupFile = $element;
    $backupFile =~ s{/}{.}g; # Replace / with .
    $backupFile = substr $backupFile, 1;
    $backupFile .= ".tar.bz2";
    $backupFile = $destination . "/" . $backupFile;

    # Do the tar command
    $message = "Backing up: $element to $backupFile\n";
    print $message;
    $mailMessage .= $message;
    system("tar --create --bzip2 --file $backupFile $element");

    # Set the owner and the permissions of the file
    chown $uid, $gid, $backupFile;
    chmod $permission, $backupFile;
}

my $endTime = time();
my $runTime = $endTime - $startTime;

# If we arrive here we send an e-mail proclaiming success. If we do not get here
# we hope that the wrapping backup.sh script sends the error as an e-mail.
$message = "Collecting data succeeded.\nThe script took $runTime seconds to run.\n";
print $message;
$mailMessage .= $message;
my @emailTo = split(/ /, $emailRecipients);
my $emailAddress;
for $emailAddress (@emailTo) {
   system("echo '$mailMessage' | mail -s 'Backup script successfully executed' $emailAddress");
}
