#!/usr/bin/perl
use warnings;
use strict;

use Config::Simple;
use POSIX 'strftime';
use File::Path;
use Date::Calc qw(:all);

# The message to mail
my $mailMessage;

# Record start time
my $startTime = time();

# Read config file
my $configFile = "config";
my $hostname = `hostname -A`;

print "##########################################################\n";
print "Fetching backup from Backing up ", $hostname, "\n";
print "Backup configuration file: ", $configFile, "\n";

my $cfg = new Config::Simple();

$cfg->read($configFile) or die $cfg->error();
my $sshUser = $cfg->param('SshUser') or die $cfg->error("SshUser not set");
my $remoteDirectory = $cfg->param('RemoteDirectory') or die $cfg->error("RemoteDirectory not set");
my $localDirectory = $cfg->param('LocalDirectory') or die $cfg->error("LocalDirectory not set");
my $emailRecipients = $cfg->param('MailRecipients') or die $cfg->error("MailRecipients not set");

# Get the local date/time
my $dtStr = strftime '%Y-%m-%d %H:%M', localtime;
my $message = "Backup fetch script started at $dtStr\n";
print $message;
$mailMessage .= $message;

# The local directory is a concatenation of the base local directory with the number of the day
# of the week. This means that a week of backups is kept before the first one is overwritten.
(my $sec, my $min,my $hour, my $mday, my $mon, my $year, my $wday, my $yday, my $isdst) = localtime(time);

# Add trailing slash if not present
if ($localDirectory !~ m#(/|\\)$#)
{
    $localDirectory .= '/';
}
$localDirectory .= $wday;

my $dow = Day_of_Week_to_Text($wday);
$message = "Destination for $dow: $localDirectory\n";
print $message;
$mailMessage .= $message;

# Create the local directory if it does not exist
if (not -d $localDirectory) {
    mkpath($localDirectory) or die print "mkpath $localDirectory failed";
}

my $remoteStr = $sshUser . ":" . $remoteDirectory;
my @output = `rsync -avz -e ssh $remoteStr $localDirectory`;
$message = "\nrsync output:\n@output\n";
print $message;
$mailMessage .= $message;

# How long did we run
my $endTime = time();
my $runTime = $endTime - $startTime;

# If we arrive here we send an e-mail proclaiming success. If we do not get here
# we hope that the wrapping backup.sh script sends the error as an e-mail.
$message = "Collecting data succeeded.\nThe script took $runTime seconds to run.\n";
print $message;
$mailMessage .= $message;

my @emailTo = split(/ /, $emailRecipients);
my $emailAddress;
for $emailAddress (@emailTo) {
   system("echo '$mailMessage' | mail -s 'Backup fetch script successfully executed' $emailAddress");
}
