# Run the perl backup fetch script. If it dies for any reason, send an email with the log file
cd /home/path_to_perl_script
./vpsbackupfetch.plx 2>script.log || mail -s 'Backup fetch script failed' postmaster@example.org < script.log
